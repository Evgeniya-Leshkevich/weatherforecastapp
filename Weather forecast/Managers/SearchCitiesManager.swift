import Foundation
import UIKit

enum Keys: String {
    case cityKey = "cityKey"
}

class SearchCitiesManager {
    
    static let shared = SearchCitiesManager()
    private init() {}
    
    func saveCities(arrayOfCities: SearchedCities) {
        var array = self.loadCities()
        array.append(arrayOfCities)
        UserDefaults.standard.set(encodable: array, forKey: Keys.cityKey.rawValue)
    }
    
    func loadCities() -> [SearchedCities] {
        guard let arrayOfCities = UserDefaults.standard.value([SearchedCities].self, forKey: Keys.cityKey.rawValue) else {
            return []
        }
        return arrayOfCities
        
    }
}



