import Foundation

class CurrentWeatherManager {
    static let shared = CurrentWeatherManager()
    
    private init() {}
    
    private let weatherBaseURL = "https://api.openweathermap.org/data/2.5/weather?"
    private let weatherAPIKey = "341474a75322d451bec94ad93f64f2e6"
    private let weatherUnits = "metric"
    private let languageCode = Locale.current.languageCode
    
    func defineUrl(city: String) -> URL? {
        guard let lang = languageCode else {
            return nil }
        guard let url = URL(string: "\(weatherBaseURL)q=\(city)&appid=\(weatherAPIKey)&units=\(weatherUnits)&lang=\(lang)".encodeUrl) else {
            return nil
        }
        return url
    }
    
    func getCurrentWeather(url: URL, completion: @escaping (CurrentWeather) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            if error == nil, let data = data {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                        
                        guard let weather = json["weather"] as? [Any] else {return}
                        if let subweather = weather[safe: 0] as? [String: Any] {
                            if let currentWeatherDescription = subweather["main"] as? String {
                                if let currentWeatherDetailedDescription = subweather["description"] as? String {
                                    if let main = json["main"] as? [String: Any] {
                                        if let temp = main["temp"] as? Double {
                                            if let feelsLike = main["feels_like"] as? Double {
                                                if let tempMax = main["temp_max"] as? Double {
                                                    if let tempMin = main["temp_min"] as? Double {
                                                        guard let city = json["name"] as? String else {return}
                                                        let currentWeather = CurrentWeather(temp: Int(temp), feelsLike: Int(feelsLike), tempMin: Int(tempMin), tempMax: Int(tempMax), weatherDescription: currentWeatherDescription, currentWeatherDetailedDescription: currentWeatherDetailedDescription, city: city)
                                                        completion(currentWeather)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch {
                    print(error)
                }
            }
        }
        task.resume()
    }
    
    
    
}

