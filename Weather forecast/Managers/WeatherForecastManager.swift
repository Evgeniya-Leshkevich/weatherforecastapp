import Foundation

class WeatherForecastManager {
    static let shared = WeatherForecastManager()
    
    private init() {}
    
    private let weatherBaseURL = "https://api.openweathermap.org/data/2.5/forecast/daily?"
    private let weatherAPIKey = "341474a75322d451bec94ad93f64f2e6"
    private let weatherNumberOfDays = "8"
    private let weatherUnits = "metric"
    private let languageCode = Locale.current.languageCode
    
    func defineUrl(city: String) -> URL? {
        guard let lang = languageCode else {
            return nil }
        guard let url = URL(string: "\(weatherBaseURL)q=\(city)&cnt=\(weatherNumberOfDays)&appid=\(weatherAPIKey)&units=\(weatherUnits)&lang=\(lang)".encodeUrl) else {
            return nil
        }
        return url
    }
    
    func getWeatherForecast (url: URL, completion: @escaping ([WeatherForecast]) -> Void) {
        var request = URLRequest(url: url)
        var weatherArray: [WeatherForecast] = []
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            if error == nil, let data = data {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                        
                        let weather = WeatherRequest()
                        let date = Date()
                        let formater = DateFormatter()
                        formater.dateFormat = "EEEE"
                        
                        guard let list = json["list"] as? [Any] else {return}
                        if let subListOne = list[safe: 0] as? [String: Any] {
                            if let temp = subListOne["temp"] as? [String: Any] {
                                if let currentWeather = temp["day"] as? Double {
                                    weather.list?[safe: 0]?.temp?.day = currentWeather
                                    if let feelsLike = subListOne["feels_like"] as? [String: Any] {
                                        if let feelsLikeDay = feelsLike["day"] as? Double {
                                            weather.list?[safe:0]?.feels_like?.day = feelsLikeDay
                                        }
                                    }
                                }
                                guard let weatherNew = subListOne["weather"] as? [Any] else {return}
                                if let subWeather = weatherNew[safe: 0] as? [String: Any] {
                                    if let currentMain = subWeather["main"] as? String {
                                        weather.list?[safe: 0]?.weather?[safe: 0]?.main = currentMain
                                    }
                                }
                            }
                        }
                        if let subListTwo = list[safe: 1] as? [String: Any] {
                            if let temp = subListTwo["temp"] as? [String: Any] {
                                if let firstDayTemp = temp["day"] as? Double {
                                    weather.list?[safe: 1]?.temp?.day = firstDayTemp
                                    if let firstNightTemp = temp["night"] as? Double {
                                        weather.list?[safe: 1]?.temp?.night = firstNightTemp
                                        
                                        guard let weatherNew = subListTwo["weather"] as? [Any] else {return}
                                        if let subWeather = weatherNew[safe: 0] as? [String: Any] {
                                            if let firstDayMain = subWeather["main"] as? String {
                                                weather.list?[safe: 1]?.weather?[safe: 0]?.main = firstDayMain
                                                if let firstDay = Calendar.current.date(byAdding: .day, value: 1, to: date){
                                                    let dateString = formater.string(from: firstDay)
                                                    let firstDayWeather = WeatherForecast(date: "\(dateString)", weatherDescription: firstDayMain, weatherDayTemp: Int(firstDayTemp), weatherNightTemp: Int(firstNightTemp))
                                                    weatherArray.append(firstDayWeather)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if let subListThree = list[safe: 2] as? [String: Any] {
                            if let temp = subListThree["temp"] as? [String: Any] {
                                if let secondDayTemp = temp["day"] as? Double {
                                    weather.list?[safe: 2]?.temp?.day = secondDayTemp
                                    if let secondNightTemp = temp["night"] as? Double {
                                        weather.list?[safe: 2]?.temp?.night = secondNightTemp
                                        
                                        guard let weatherNew = subListThree["weather"] as? [Any] else {return}
                                        if let subWeather = weatherNew[safe: 0] as? [String: Any] {
                                            if let secondDayMain = subWeather["main"] as? String {
                                                weather.list?[safe: 2]?.weather?[safe: 0]?.main = secondDayMain
                                                
                                                if let secondDay = Calendar.current.date(byAdding: .day, value: 2, to: date){
                                                    let dateString = formater.string(from: secondDay)
                                                    let secondDayWeather = WeatherForecast(date: "\(dateString)", weatherDescription: secondDayMain, weatherDayTemp: Int(secondDayTemp), weatherNightTemp: Int(secondNightTemp))
                                                    weatherArray.append(secondDayWeather)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if let subListFour = list[safe: 3] as? [String: Any] {
                            if let temp = subListFour["temp"] as? [String: Any] {
                                if let thirdDayTemp = temp["day"] as? Double {
                                    weather.list?[safe: 3]?.temp?.day = thirdDayTemp
                                    if let thirdNightTemp = temp["night"] as? Double {
                                        weather.list?[safe: 3]?.temp?.night = thirdNightTemp
                                        
                                        guard let weatherNew = subListFour["weather"] as? [Any] else {return}
                                        if let subWeather = weatherNew[safe: 0] as? [String: Any] {
                                            if let thirdDayMain = subWeather["main"] as? String {
                                                weather.list?[safe: 3]?.weather?[safe: 0]?.main = thirdDayMain
                                                
                                                if let thirdDay = Calendar.current.date(byAdding: .day, value: 3, to: date){
                                                    let dateString = formater.string(from: thirdDay)
                                                    let thirdDayWeather = WeatherForecast(date: "\(dateString)", weatherDescription: thirdDayMain, weatherDayTemp: Int(thirdDayTemp), weatherNightTemp: Int(thirdNightTemp))
                                                    weatherArray.append(thirdDayWeather)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if let subListFive = list[safe: 4] as? [String: Any] {
                            if let temp = subListFive["temp"] as? [String: Any] {
                                if let fourthDayTemp = temp["day"] as? Double {
                                    weather.list?[safe: 4]?.temp?.day = fourthDayTemp
                                    if let fourthNightTemp = temp["night"] as? Double {
                                        weather.list?[safe: 4]?.temp?.night = fourthNightTemp
                                        
                                        guard let weatherNew = subListFive["weather"] as? [Any] else {return}
                                        if let subWeather = weatherNew[safe: 0] as? [String: Any] {
                                            if let fourthDayMain = subWeather["main"] as? String {
                                                weather.list?[safe: 4]?.weather?[safe: 0]?.main = fourthDayMain
                                                
                                                if let fourthDay = Calendar.current.date(byAdding: .day, value: 4, to: date){
                                                    let dateString = formater.string(from: fourthDay)
                                                    let fourthDayWeather = WeatherForecast(date: "\(dateString)", weatherDescription: fourthDayMain, weatherDayTemp: Int(fourthDayTemp), weatherNightTemp: Int(fourthNightTemp))
                                                    weatherArray.append(fourthDayWeather)
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if let subListSix = list[safe: 5] as? [String: Any] {
                            if let temp = subListSix["temp"] as? [String: Any] {
                                if let fifthDayTemp = temp["day"] as? Double {
                                    weather.list?[safe: 5]?.temp?.day = fifthDayTemp
                                    if let fifthNightTemp = temp["night"] as? Double {
                                        weather.list?[safe: 5]?.temp?.night = fifthNightTemp
                                        
                                        guard let weatherNew = subListSix["weather"] as? [Any] else {return}
                                        if let subWeather = weatherNew[safe: 0] as? [String: Any] {
                                            if let fifthDayMain = subWeather["main"] as? String {
                                                weather.list?[safe: 5]?.weather?[safe: 0]?.main = fifthDayMain
                                                
                                                if let fifthDay = Calendar.current.date(byAdding: .day, value: 5, to: date){
                                                    let dateString = formater.string(from: fifthDay)
                                                    let fifthDayWeather = WeatherForecast(date: "\(dateString)", weatherDescription: fifthDayMain, weatherDayTemp: Int(fifthDayTemp), weatherNightTemp: Int(fifthNightTemp))
                                                    weatherArray.append(fifthDayWeather)
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if let subListSeven = list[safe: 6] as? [String: Any] {
                            if let temp = subListSeven["temp"] as? [String: Any] {
                                if let sixthDayTemp = temp["day"] as? Double {
                                    weather.list?[safe: 6]?.temp?.day = sixthDayTemp
                                    if let sixthNightTemp = temp["night"] as? Double {
                                        weather.list?[safe: 6]?.temp?.night = sixthNightTemp
                                        
                                        guard let weatherNew = subListSeven["weather"] as? [Any] else {return}
                                        if let subWeather = weatherNew[safe: 0] as? [String: Any] {
                                            if let sixthDayMain = subWeather["main"] as? String {
                                                weather.list?[safe: 6]?.weather?[safe: 0]?.main = sixthDayMain
                                                
                                                if let sixthDay = Calendar.current.date(byAdding: .day, value: 6, to: date){
                                                    let dateString = formater.string(from: sixthDay)
                                                    let sixthDayWeather = WeatherForecast(date: "\(dateString)", weatherDescription: sixthDayMain, weatherDayTemp: Int(sixthDayTemp), weatherNightTemp: Int(sixthNightTemp))
                                                    weatherArray.append(sixthDayWeather)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if let subListEight = list[safe: 7] as? [String: Any] {
                            if let temp = subListEight["temp"] as? [String: Any] {
                                if let seventhDayTemp = temp["day"] as? Double {
                                    weather.list?[safe: 7]?.temp?.day = seventhDayTemp
                                    if let seventhNightTemp = temp["night"] as? Double {
                                        weather.list?[safe: 7]?.temp?.night = seventhNightTemp
                                        
                                        guard let weatherNew = subListEight["weather"] as? [Any] else {return}
                                        if let subWeather = weatherNew[safe: 0] as? [String: Any] {
                                            if let seventhDayMain = subWeather["main"] as? String {
                                                weather.list?[safe: 7]?.weather?[safe: 0]?.main = seventhDayMain
                                                
                                                if let seventhDay = Calendar.current.date(byAdding: .day, value: 7, to: date){
                                                    let dateString = formater.string(from: seventhDay)
                                                    let seventhDayWeather = WeatherForecast(date: "\(dateString)", weatherDescription: seventhDayMain, weatherDayTemp: Int(seventhDayTemp), weatherNightTemp: Int(seventhNightTemp))
                                                    weatherArray.append(seventhDayWeather)
                                                    completion(weatherArray)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch {
                    print(error)
                }
            }
        }
        task.resume()
    }
    
    
}
