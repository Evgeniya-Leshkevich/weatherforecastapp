import Foundation
import UIKit
import CoreLocation

class WeatherViewModel: NSObject, CLLocationManagerDelegate {
    
    // MARK: - let
    private let locationManager = CLLocationManager()
    private let geocoder = CLGeocoder()
    
    // MARK: - var
    var lat = 0.0
    var lon = 0.0
    var city = Bindable<String?> (nil)
    var weatherForecastArray = Bindable<[WeatherForecast]?> (nil)
    var tempLabel = Bindable<String?> (nil)
    var maxTempValue = Bindable<String?> (nil)
    var minTempValue = Bindable<String?> (nil)
    var weatherDescriptionLabel = Bindable<String?> (nil)
    var cityLabel = Bindable<String?> (nil)
    var feelsLikeLabel = Bindable<String?> (nil)
    var maxTempLabel = Bindable<String?> (nil)
    var minTempLabel = Bindable<String?> (nil)
    var weatherImageView = Bindable<UIImage?>(nil)
    var weatherBackgroundImage = Bindable<UIImage?>(nil)
    
    // MARK: - VC life Func
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    // MARK: - FLow func
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if self.lat == 0.0 && self.lon == 0.0 {
            let location: CLLocationCoordinate2D = manager.location!.coordinate
            self.lat = location.latitude
            self.lon = location.longitude
            self.geocode(latitude: self.lat, longitude: self.lon) { [weak self] (placemark, error) in
                guard let placemark = placemark, error == nil else { return }
                DispatchQueue.main.async {
                    if let city = placemark.locality {
                        self?.city.value = city
                        self?.getCurrentWeather(city: self?.city.value ?? "")
                        self?.getWeatherForecast(city: self?.city.value ?? "")
                    }
                }
            }
        }
    }
    
    private func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        self.geocoder.reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    
    func getWeatherForecast(city: String) {
        guard let url = WeatherForecastManager.shared.defineUrl(city: city) else {return}
        WeatherForecastManager.shared.getWeatherForecast(url: url) { [weak self] (weatherArray) in
            DispatchQueue.main.async {
                self?.weatherForecastArray.value = weatherArray
            }
        }
    }
    
    func getCurrentWeather(city: String) {
        guard let url = CurrentWeatherManager.shared.defineUrl(city: city) else {return}
        CurrentWeatherManager.shared.getCurrentWeather(url: url) { [weak self] (currentWeather) in
            DispatchQueue.main.async {
                self?.tempLabel.value = "\(currentWeather.temp)℃"
                self?.maxTempValue.value = "\(currentWeather.tempMax)℃"
                self?.minTempValue.value = "\(currentWeather.tempMin)℃"
                self?.weatherDescriptionLabel.value = "\(currentWeather.currentWeatherDetailedDescription)"
                self?.setWeatherDescriptionImage(weatherDescription: currentWeather.weatherDescription)
                self?.setBackgroundImage(weatherDescription: currentWeather.weatherDescription)
                self?.feelsLikeLabel.value = "\(NSLocalizedString("feels like", comment: "")) \(Int(currentWeather.feelsLike))℃"
                self?.maxTempLabel.value = "max".localized
                self?.minTempLabel.value = "min".localized
                self?.cityLabel.value = "\(currentWeather.city)"
            }
        }
    }
    
    private func setWeatherDescriptionImage (weatherDescription : String) {
        switch weatherDescription {
        case "Thunderstorm": self.weatherImageView.value = UIImage(named: "Thunderstorm")
        case "Drizzle": self.weatherImageView.value = UIImage(named: "Drizzle")
        case "Rain": self.weatherImageView.value = UIImage(named: "Rain")
        case "Snow": self.weatherImageView.value = UIImage(named: "Snow")
        case "Mist": self.weatherImageView.value = UIImage(named: "Mist")
        case "Smoke": self.weatherImageView.value = UIImage(named: "Smoke")
        case "Haze": self.weatherImageView.value = UIImage(named: "Haze")
        case "Dust": self.weatherImageView.value = UIImage(named: "Dust")
        case "Fog": self.weatherImageView.value = UIImage(named: "Fog")
        case "Sand": self.weatherImageView.value = UIImage(named: "Sand")
        case "Ash": self.weatherImageView.value = UIImage(named: "Ash")
        case "Squall": self.weatherImageView.value = UIImage(named: "Squall")
        case "Tornado": self.weatherImageView.value = UIImage(named: "Tornado")
        case "Clouds": self.weatherImageView.value = UIImage(named: "Clouds")
        default: self.weatherImageView.value = UIImage(named: "Clear")
        }
    }
    
    private func setBackgroundImage(weatherDescription : String) {
        switch weatherDescription {
        case "Thunderstorm": self.weatherBackgroundImage.value = UIImage(named: "darkBlueBg")
        case "Drizzle": self.weatherBackgroundImage.value = UIImage(named: "blueBg")
        case "Rain": self.weatherBackgroundImage.value = UIImage(named: "blueBg")
        case "Snow": self.weatherBackgroundImage.value = UIImage(named: "grayBg")
        case "Mist": self.weatherBackgroundImage.value = UIImage(named: "grayBg")
        case "Smoke": self.weatherBackgroundImage.value = UIImage(named: "grayBg")
        case "Haze": self.weatherBackgroundImage.value = UIImage(named: "grayBg")
        case "Dust": self.weatherBackgroundImage.value = UIImage(named: "grayBg")
        case "Fog":  self.weatherBackgroundImage.value = UIImage(named: "grayBg")
        case "Sand":  self.weatherBackgroundImage.value = UIImage(named: "blueBg")
        case "Ash":  self.weatherBackgroundImage.value = UIImage(named: "grayBg")
        case "Squall":  self.weatherBackgroundImage.value = UIImage(named: "darkBlueBg")
        case "Tornado": self.weatherBackgroundImage.value = UIImage(named: "darkBlueBg")
        case "Clouds": self.weatherBackgroundImage.value = UIImage(named: "blueBg")
        default: self.weatherBackgroundImage.value = UIImage(named: "blueBg")
        }
    }
    
}



