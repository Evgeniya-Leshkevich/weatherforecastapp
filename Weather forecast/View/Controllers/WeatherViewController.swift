import UIKit

class WeatherViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var minTempValue: UILabel!
    @IBOutlet weak var weatherForecastTableView: UITableView!
    @IBOutlet weak var weatherConditionView: UIView!
    @IBOutlet weak var maxTempValue: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    // MARK: - var
    var weatherViewModel: WeatherViewModel?
    var city = ""
    var weatherForecastArray: [WeatherForecast] = []
    
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.weatherViewModel = WeatherViewModel()
        self.bindOutlets()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.spinner.hidesWhenStopped = true
        self.spinner.startAnimating()
        self.getWeather()
    }
    
    // MARK: - IBAction
    @IBAction func burgerButtonPressed(_ sender: UIButton) {
        self.goToSearchViewController()
    }
    
    // MARK: - FLow func
    private func bindOutlets() {
        self.weatherViewModel?.weatherForecastArray.bind({ [weak self] (weatherForecastArray) in
            if let weatherForecastArray = weatherForecastArray {
                self?.weatherForecastArray = weatherForecastArray
                self?.weatherForecastTableView.reloadData()
                self?.spinner.stopAnimating()
            }
        })
        
        self.weatherViewModel?.tempLabel.bind({ [weak self] (tempLabel) in
            self?.tempLabel.text = tempLabel
        })
        
        self.weatherViewModel?.maxTempValue.bind({ [weak self] (maxTempValue) in
            self?.maxTempValue.text = maxTempValue
        })
        
        self.weatherViewModel?.minTempValue.bind({ [weak self] (minTempValue) in
            self?.minTempValue.text = minTempValue
        })
        
        self.weatherViewModel?.weatherDescriptionLabel.bind({ [weak self] (weatherDescriptionLabel) in
            self?.weatherDescriptionLabel.text = weatherDescriptionLabel
        })
        
        self.weatherViewModel?.feelsLikeLabel.bind({ [weak self] (feelsLikeLabel) in
            self?.feelsLikeLabel.text = feelsLikeLabel
        })
        
        self.weatherViewModel?.maxTempLabel.bind({ [weak self] (maxTempLabel) in
            self?.maxTempLabel.text = maxTempLabel
        })
        
        self.weatherViewModel?.minTempLabel.bind({ [weak self] (minTempLabel) in
            self?.minTempLabel.text = minTempLabel
        })
        
        self.weatherViewModel?.cityLabel.bind({ [weak self] (cityLabel) in
            self?.cityLabel.text = cityLabel
        })
        
        self.weatherViewModel?.weatherImageView.bind({ [weak self] (weatherImageView) in
            self?.weatherImageView.image = weatherImageView
        })
        
        self.weatherViewModel?.weatherBackgroundImage.bind({ [weak self] (weatherBackgroundImage) in
            self?.backgroundImageView.image = weatherBackgroundImage
            self?.backgroundImageView.contentMode = .center
            self?.backgroundImageView.clipsToBounds = false
            self?.backgroundImageView.addParalaxEffect()
        })
    }
    
    private func getWeather() {
        self.weatherViewModel?.getCurrentWeather(city: self.city)
        self.weatherViewModel?.getWeatherForecast(city: self.city)
        self.spinner.stopAnimating()
    }
    
    private func goToSearchViewController() {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController else{
            return
        }
        controller.backgroundImageView = self.backgroundImageView.image
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
// MARK: - extension
extension WeatherViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.weatherForecastArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherTableViewCell", for: indexPath) as? WeatherTableViewCell
        else {
            return UITableViewCell()
        }
        cell.configure(object: self.weatherForecastArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension WeatherViewController: SearchViewControllerDelegate {
    func defineCity(city: String) {
        self.city = city
    }
}





