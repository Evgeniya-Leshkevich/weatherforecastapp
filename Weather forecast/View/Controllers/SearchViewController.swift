import UIKit

protocol SearchViewControllerDelegate: AnyObject {
    func defineCity(city: String)
}

class SearchViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var citySearchText: UITextField!
    @IBOutlet weak var weatherCollectionView: UICollectionView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    // MARK: - let
    private let itemPerRow: CGFloat = 2
    private let sectionInserts = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    private let numberOfSavedCities = 6
    
    // MARK: - var
    weak var delegate: SearchViewControllerDelegate?
    var arrayOfCities = SearchCitiesManager.shared.loadCities()
    var backgroundImageView: UIImage?
    
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.arrayOfCities.reverse()
        self.searchView.layer.cornerRadius = 15
        self.backgroundImage.image = self.backgroundImageView
        self.citySearchText.placeholder = "Enter a city to see the weather".localized
    }
    
    // MARK: - IBAction
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

// MARK: - extension
extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let city = citySearchText.text {
            let currentCity = SearchedCities(city: city)
            SearchCitiesManager.shared.saveCities(arrayOfCities: currentCity)
            self.arrayOfCities.insert(currentCity, at: 0)
            self.weatherCollectionView.reloadData()
        }
        citySearchText.text = ""
        textField.resignFirstResponder()
        return true
    }
}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayOfCities.prefix(self.numberOfSavedCities).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherCollectionViewCell", for: indexPath) as? WeatherCollectionViewCell
        else {
            return UICollectionViewCell() }
        cell.configure(object: self.arrayOfCities[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let city = self.arrayOfCities[indexPath.row].city {
            self.delegate?.defineCity(city: city)
        }
        self.navigationController?.popViewController(animated: true)
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingWidth = sectionInserts.left * (itemPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingWidth
        let widthPerItem = availableWidth / itemPerRow
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInserts
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInserts.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInserts.left
    }
    
}
