import UIKit

class WeatherTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherDescriptionView: UIImageView!
    @IBOutlet weak var dayTempLabel: UILabel!
    @IBOutlet weak var nightTempLabel: UILabel!
    
    // MARK: - FLow func
    func configure (object: WeatherForecast) {
        self.dateLabel.text = object.date
        self.dayTempLabel.text = "\(object.weatherDayTemp)"
        self.nightTempLabel.text = "\(object.weatherNightTemp)"
        self.weatherDescriptionView.image = UIImage(named: "\(object.weatherDescription)")
    }
}
