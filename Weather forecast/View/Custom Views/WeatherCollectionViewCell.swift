import UIKit

class WeatherCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    // MARK: - VC life Func
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 30
    }
    
    // MARK: - FLow func
    func configure (object: SearchedCities) {
        if let city = object.city {
            if let url = CurrentWeatherManager.shared.defineUrl(city: city) {
                CurrentWeatherManager.shared.getCurrentWeather(url: url) { [weak self] (currentWeather) in
                    DispatchQueue.main.async {
                        self?.spinner.hidesWhenStopped = true
                        self?.spinner.startAnimating()
                        self?.cityLabel.text = currentWeather.city
                        self?.tempLabel.text = "\(currentWeather.temp)º"
                        self?.weatherDescriptionLabel.text = currentWeather.currentWeatherDetailedDescription
                        self?.setWeatherDescriptionImage(weatherDescription: currentWeather.weatherDescription)
                        self?.spinner.stopAnimating()
                    }
                }
            }
        }
    }
    
    func setWeatherDescriptionImage (weatherDescription : String) {
        switch weatherDescription {
        case "Thunderstorm": self.weatherImageView.image = UIImage(named: "Thunderstorm")
        case "Drizzle": self.weatherImageView.image = UIImage(named: "Drizzle")
        case "Rain": self.weatherImageView.image = UIImage(named: "Rain")
        case "Snow": self.weatherImageView.image = UIImage(named: "Snow")
        case "Mist": self.weatherImageView.image = UIImage(named: "Mist")
        case "Smoke": self.weatherImageView.image = UIImage(named: "Smoke")
        case "Haze": self.weatherImageView.image = UIImage(named: "Haze")
        case "Dust": self.weatherImageView.image = UIImage(named: "Dust")
        case "Fog": self.weatherImageView.image = UIImage(named: "Fog")
        case "Sand": self.weatherImageView.image = UIImage(named: "Sand")
        case "Ash": self.weatherImageView.image = UIImage(named: "Ash")
        case "Squall": self.weatherImageView.image = UIImage(named: "Squall")
        case "Tornado": self.weatherImageView.image = UIImage(named: "Tornado")
        case "Clouds": self.weatherImageView.image = UIImage(named: "Clouds")
        default: self.weatherImageView.image = UIImage(named: "Clear")
        }
    }
}
