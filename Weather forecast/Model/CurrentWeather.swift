import Foundation
import UIKit

class CurrentWeather {
    var temp: Int
    var feelsLike: Int
    var tempMin: Int
    var tempMax: Int
    var weatherDescription: String
    var currentWeatherDetailedDescription: String
    var city: String
    
    init(temp: Int, feelsLike: Int, tempMin: Int, tempMax: Int, weatherDescription: String, currentWeatherDetailedDescription: String, city: String) {
        self.temp = temp
        self.feelsLike = feelsLike
        self.tempMin = tempMin
        self.tempMax = tempMax
        self.weatherDescription = weatherDescription
        self.city = city
        self.currentWeatherDetailedDescription = currentWeatherDetailedDescription
    }
}


