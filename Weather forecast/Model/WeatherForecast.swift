import Foundation
import UIKit

class WeatherForecast {
    var date: String
    var weatherDescription: String
    var weatherDayTemp: Int
    var weatherNightTemp: Int
    
    init(date: String, weatherDescription: String, weatherDayTemp: Int, weatherNightTemp: Int) {
        self.date = date
        self.weatherDescription = weatherDescription
        self.weatherDayTemp = weatherDayTemp
        self.weatherNightTemp = weatherNightTemp
    }
}
