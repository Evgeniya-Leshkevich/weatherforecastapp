import Foundation

class WeatherRequest {
    var city: CityInfo?
    var cod: String?
    var message: Double?
    var cnt: Int?
    var list: [DailyWeather]?
}

class CityInfo {
    var id: Int?
    var name: String?
    var coord: CoordObjects?
    var country: String?
    var population: Int?
    var timezone: Int?
}

class CoordObjects {
    var lon: Double?
    var lat: Double?
}

class DailyWeather {
    var dt: Int?
    var sunrise: Int?
    var sunset: Int?
    var temp: TempObjects?
    var feels_like: FeelsLikeObjects?
    var pressure: Int?
    var humidity: Int?
    var weather: [WeatherObjects]?
    var speed: Double?
    var deg: Double?
    var clouds: Int?
    var pop: Double?
    var rain: Double?
}

class TempObjects {
    var day: Double?
    var min: Double?
    var max: Double?
    var night: Double?
    var eve: Double?
    var morn: Double?
}

class FeelsLikeObjects {
    var day: Double?
    var night: Double?
    var eve: Double?
    var morn: Double?
}

class WeatherObjects {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
}



