import Foundation
import UIKit

class SearchedCities: Codable {
    var city: String?
    
    init(city: String?) {
        self.city = city
    }
    
    private enum CodingKeys: String, CodingKey {
        case city
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        city = try container.decodeIfPresent(String.self, forKey: .city)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.city, forKey: .city)
    }
    
}
